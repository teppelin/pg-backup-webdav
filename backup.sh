#! /bin/sh

set -e

check_env()
{
	eval VAR="\$$1";
	if [ -z "$VAR" ]; then
		echo "You need to set the $1 environment variable.";
		exit 1;
	fi
}

# Make sure the env vars are set
check_env WEBDAV_USERNAME
check_env WEBDAV_PASSWORD
check_env WEBDAV_ENDPOINT
check_env DATABASE_URL

POSTGRES_HOST_OPTS="-d $DATABASE_URL $POSTGRES_EXTRA_OPTS"

echo "Creating a dump..."

pg_dump $POSTGRES_HOST_OPTS > dump.sql.gz

echo "Uploading dump to $WEBDAV_ENDPOINT"

curl -u $WEBDAV_USERNAME:$WEBDAV_PASSWORD -T dump.sql.gz $WEBDAV_ENDPOINT/$(date +"%Y-%m-%dT%H:%M:%SZ").sql.gz || exit 2

echo "SQL backup uploaded successfully"

