# postgres-backup-webdav

Dump pgsql and upload to some cloud that implements the webdav protocol.

## Usage

Docker:
```sh
λ docker run \
> -e DATABASE_URL=postgresql://localhost/mydb \
> -e WEBDAV_USERNAME=u \
> -e WEBDAV_PASSWORD=sekret \
> -e WEBDAV_ENDPOINT=https://cloud.example.com/remote.php/dav/files/user/dumps \
> teppelin/postgres-backup-webdav
```

### Environment Variables

| env variable | description |
|--|--|
| HEALTHCHECK_PORT | Port listening for cron-schedule health check. Defaults to `8080`. |
| DATABASE_URL | [PostgreSQL Connection URI](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING). E.g postgresql://user:secret@localhost/mydb |
| POSTGRES_EXTRA_OPTS | Additional options for `pg_dump`. Defaults to `-Z9`. |
| SCHEDULE | [Cron-schedule](http://godoc.org/github.com/robfig/cron#hdr-Predefined_schedules) specifying the interval between postgres backups. Defaults to `@daily`. |
| WEBDAV_USERNAME | Your WebDAV username |
| WEBDAV_PASSWORD | Your WebDAV password |
| WEBDAV_ENDPOINT | The full endpoint with including the directory where you want the dump saved. **NO TRAILING SLASH**. E.g. https://cloud.example.com/remote.php/dav/files/user/dumps |

